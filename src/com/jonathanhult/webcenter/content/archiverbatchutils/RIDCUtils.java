package com.jonathanhult.webcenter.content.archiverbatchutils;

import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchMain.CFG_PORT;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchMain.CFG_SERVER;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.IDC_PROTOCOL;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import oracle.stellent.ridc.IdcClientException;
import oracle.stellent.ridc.IdcClientManager;
import oracle.stellent.ridc.IdcContext;
import oracle.stellent.ridc.model.DataBinder;
import oracle.stellent.ridc.protocol.ServiceResponse;
import oracle.stellent.ridc.protocol.intradoc.IntradocClient;

public class RIDCUtils {

	public static IdcClientManager m_idcClientManager;
	public static IntradocClient m_idcClient;

	/**
	 * This method executes a service.
	 *
	 * @param binder
	 *            DataBinder containing service parameters.
	 * @param userName
	 *            User to execute service as.
	 * @return DataBinder from service call
	 * @throws IdcClientException
	 * @throws IOException
	 */
	public static DataBinder executeService(final DataBinder binder, final String userName) throws IdcClientException {
		// Execute the request
		final ServiceResponse response = getIntradocClient().sendRequest(new IdcContext(userName), binder);
		// System.out.println("Successfully called service");

		// Get the response as a DataBinder
		return response.getResponseAsBinder();
	}

	/**
	 * This method returns IdcClientManager.
	 *
	 * @return IdcClientManager
	 */
	public static IdcClientManager getIdcClientManager() {
		if (m_idcClientManager == null) {
			// Needed to create IntradocClient
			m_idcClientManager = new IdcClientManager();
		}
		return m_idcClientManager;
	}

	/**
	 * This method returns a IntradocClient object.
	 *
	 * @return IntradocClient
	 * @throws IdcClientException
	 */
	public static IntradocClient getIntradocClient() throws IdcClientException {
		if (m_idcClient == null) {
			// Client to talk to WebCenter Content
			m_idcClient = (IntradocClient) getIdcClientManager()
					.createClient(IDC_PROTOCOL + CFG_SERVER + ":" + CFG_PORT);
		}
		return m_idcClient;
	}

	/**
	 * This method returns a new DataBinder.
	 *
	 * @return New DataBinder
	 * @throws IdcClientException
	 */
	public static DataBinder getNewDataBinder() throws IdcClientException {
		return getIntradocClient().createBinder();
	}

	public static Map<String, String> mergeIntoMap(List<String> list1, List<String> list2) {
		Map<String, String> toReturn = new HashMap<>();

		Iterator<String> i1 = list1.iterator();
		Iterator<String> i2 = list2.iterator();
		while (i1.hasNext() && i2.hasNext()) {
			toReturn.put(i1.next(), i2.next());
		}
		if (i1.hasNext() || i2.hasNext())
			System.out.println("Mismatched sized lists:   list1=" + list1.size() + "   list2=" + list2.size());

		return toReturn;
	}
}
