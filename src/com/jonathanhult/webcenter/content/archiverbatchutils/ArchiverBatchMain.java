package com.jonathanhult.webcenter.content.archiverbatchutils;

import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.ACTION;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.ARCHIVE_DIR;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.ARCHIVE_NAME;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.A_BATCH_FILE;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.BATCH_START_NUMBER;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.CFG_OPTIONS;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.DO_IMPORT;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.IDC_NAME;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.PORT;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.SERVER;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.SERVICE;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import oracle.stellent.ridc.IdcClientException;
import oracle.stellent.ridc.model.DataObject;
import oracle.stellent.ridc.model.DataResultSet;

/**
 * 
 * @author Jonathan Hult
 *
 */
public class ArchiverBatchMain {

	// Instance/archive specific
	private static final Properties config;
	static {
		config = new Properties();
		try {
			config.load(new FileInputStream(CFG_OPTIONS));
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}
	static final String CFG_SERVER = config.getProperty(SERVER);
	static final String CFG_PORT = config.getProperty(PORT);
	static final String CFG_IDC_NAME = config.getProperty(IDC_NAME);
	static final String CFG_ARCHIVE_DIR = config.getProperty(ARCHIVE_DIR);
	static final String CFG_ARCHIVE_NAME = config.getProperty(ARCHIVE_NAME);

	static final String CFG_ACTION = config.getProperty(ACTION);
	static final String CFG_SERVICE = config.getProperty(SERVICE);
	static final boolean CFG_DO_IMPORT = Boolean.valueOf(config.getProperty(DO_IMPORT));

	private static final int CFG_BATCH_START_NUMBER = Integer.valueOf(config.getProperty(BATCH_START_NUMBER));

	/**
	 * @param args
	 * @throws IdcClientException
	 * @throws IOException
	 */
	public static void main(final String[] args) throws IdcClientException {
		System.out.println("ArchiverBatchUtils start");
		System.out.println("---------------------------------------------------------------------------------------");

		// Get BatchFiles
		final DataResultSet batchFileList = BatchUtils.getBatchFiles(CFG_ARCHIVE_NAME);

		// names of batch HDA files
		final List<String> batchFileNames = new ArrayList<>();

		int counter = 1;
		for (final DataObject row : batchFileList.getRows()) {
			if (counter > CFG_BATCH_START_NUMBER) {
				final String batchFileName = row.get(A_BATCH_FILE);
				batchFileNames.add(batchFileName);
			}
			counter++;
		}

		for (final String aBatchFile : batchFileNames) {
			// BatchUtils.findBatchByFieldAndValue(aBatchFile, D_DOC_NAME,
			// "PROD2048783");
			BatchUtils.processBatch(aBatchFile);
		}

		System.out.println("---------------------------------------------------------------------------------------");
		System.out.println("ArchiverBatchUtils end");
	}
}
