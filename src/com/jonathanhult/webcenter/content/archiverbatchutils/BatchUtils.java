package com.jonathanhult.webcenter.content.archiverbatchutils;

import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchMain.CFG_ACTION;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchMain.CFG_ARCHIVE_DIR;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchMain.CFG_ARCHIVE_NAME;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchMain.CFG_DO_IMPORT;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchMain.CFG_IDC_NAME;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchMain.CFG_SERVICE;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.ACTION;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.A_ARCHIVE_NAME;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.A_BATCH_FILE;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.BATCH_FIELD_NAME;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.BATCH_FILES;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.BATCH_VALUES;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.DATA_SOURCE;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.D_DOC_NAME;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.D_ID;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.D_RENDITION_1;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.D_RENDITION_2;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.EXPORT_RESULTS;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.GET_BATCHFILES;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.GET_BATCH_FILE_DOCUMENTS;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.GET_BATCH_VALUES;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.GET_DATARESULTSET;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.IDC_NAME;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.IDC_SERVICE;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.NUM_ROWS;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.PRIMARY_FILE;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.RESULTS;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.RESULT_NAME;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.REVISIONS;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.SLASH;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.START_ROW;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.USER;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.WEB_VIEWABLE_FILE;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.WHERE_CLAUSE;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.fieldsToExclude;
import static com.jonathanhult.webcenter.content.archiverbatchutils.RIDCUtils.executeService;
import static com.jonathanhult.webcenter.content.archiverbatchutils.RIDCUtils.getNewDataBinder;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import oracle.stellent.ridc.IdcClientException;
import oracle.stellent.ridc.model.DataBinder;
import oracle.stellent.ridc.model.DataObject;
import oracle.stellent.ridc.model.DataResultSet;
import oracle.stellent.ridc.model.DataResultSet.Field;
import oracle.stellent.ridc.model.impl.DataResultSetRow;

/*
 * @author Jonathan Hult
 */
public class BatchUtils {

	/**
	 * 
	 * @param aBatchFile
	 * @throws IdcClientException
	 */
	public static void processBatch(String aBatchFile) throws IdcClientException {
		System.out.println("---------------------------------------------------------------------------------------");
		System.out.println("Processing batchFile: " + aBatchFile);

		/*-
		// Get all dIDs from batch file
		final List<String> dIDs = getFieldValuesFromBatch(aBatchFile, D_ID);
		
		// Get all content IDs (dDocName) from batch file
		final List<String> dDocNames = getFieldValuesFromBatch(aBatchFile, D_DOC_NAME);
		
		Map<String, String> ids = mergeIntoMap(dIDs, dDocNames);
		*/
		DataResultSet items = getMetadataFromBatch(aBatchFile);

		final ExecutorService executor = Executors.newFixedThreadPool(18);

		List<Field> fields = items.getFields();
		for (DataObject rowObj : items.getRows()) {
			final Runnable worker = new FindAndImportWorker((DataResultSetRow) rowObj, fields, aBatchFile);
			executor.execute(worker);
		}

		executor.shutdown();
		// Wait until all threads are finished
		while (!executor.isTerminated()) {
		}
	}

	/**
	 * Get batch file names for a specified archive.
	 * 
	 * @param aArchiveName
	 * @return BatchFiles ResultSet
	 * @throws IdcClientException
	 */
	public static DataResultSet getBatchFiles(final String aArchiveName) throws IdcClientException {
		DataBinder request = getNewDataBinder();

		request.putLocal(IDC_SERVICE, GET_BATCHFILES);
		request.putLocal(IDC_NAME, CFG_IDC_NAME);
		request.putLocal(A_ARCHIVE_NAME, aArchiveName);

		request = executeService(request, USER);
		return request.getResultSet(BATCH_FILES);
	}

	/**
	 * 
	 * @param aBatchFile
	 * @param fieldName
	 * @param fieldValue
	 * @throws IdcClientException
	 */
	public static void findBatchByFieldAndValue(String aBatchFile, String fieldName, String fieldValue)
			throws IdcClientException {
		final List<String> values = getFieldValuesFromBatch(aBatchFile, fieldName);

		if (values.contains(fieldValue)) {
			System.out.println("Found " + fieldName + " (" + fieldValue + ")  in batch: " + aBatchFile);
		}
	}

	/**
	 * Get all field values (for fieldName) from specified batch file.
	 *
	 * @param aBatchFile
	 * @param fieldName
	 * @throws IdcClientException
	 */
	public static List<String> getFieldValuesFromBatch(final String aBatchFile, String fieldName)
			throws IdcClientException {
		final DataBinder request = getNewDataBinder();

		request.putLocal(IDC_SERVICE, GET_BATCH_VALUES);
		request.putLocal(IDC_NAME, CFG_IDC_NAME);
		request.putLocal(A_ARCHIVE_NAME, CFG_ARCHIVE_NAME);
		request.putLocal(A_BATCH_FILE, aBatchFile);
		request.putLocal(BATCH_FIELD_NAME, fieldName);

		final DataBinder response = executeService(request, USER);
		return response.getOptionList(BATCH_VALUES);
	}

	/**
	 * Get metadata values from specified batch file.
	 *
	 * @param aBatchFile
	 * @param fieldName
	 * @throws IdcClientException
	 */
	public static DataResultSet getMetadataFromBatch(final String aBatchFile) throws IdcClientException {
		int startRow = 0;
		int maxPerPage = 50;

		DataBinder request = getIDsFromBatch(aBatchFile, startRow);

		DataResultSet data = request.getResultSet(EXPORT_RESULTS);
		DataResultSet allData = request.getResultSet(EXPORT_RESULTS);

		int rowsInBatch = Integer.parseInt(request.getLocal(NUM_ROWS));
		System.out.println("rows in batch: " + rowsInBatch);

		while (rowsInBatch > startRow + maxPerPage) {
			startRow = startRow + maxPerPage;

			request = getIDsFromBatch(aBatchFile, startRow);
			data = request.getResultSet(EXPORT_RESULTS);

			for (DataObject row : data.getRows()) {
				allData.addRow(row);
			}
		}
		int totalRows = allData.getRows().size();
		System.out.println("found metadata for all rows? " + (rowsInBatch == totalRows));
		return allData;
	}

	/**
	 * Get dID/dDocName from specified batch file.
	 *
	 * @param aBatchFile
	 * @param fieldName
	 * @throws IdcClientException
	 */
	public static DataBinder getIDsFromBatch(final String aBatchFile, final int startRow) throws IdcClientException {
		final DataBinder request = getNewDataBinder();

		request.putLocal(IDC_SERVICE, GET_BATCH_FILE_DOCUMENTS);
		request.putLocal(IDC_NAME, CFG_IDC_NAME);
		request.putLocal(A_ARCHIVE_NAME, CFG_ARCHIVE_NAME);
		request.putLocal(A_BATCH_FILE, aBatchFile);
		request.putLocal(START_ROW, Integer.toString(startRow));

		return executeService(request, USER);
	}

	/**
	 * Find if content item exists in system by calling GET_DATARESULTSET. Will
	 * return a DataResultSet of size 0 if content item does not exist.
	 *
	 * @param field
	 * @param value
	 * @return DataResultSet containing results
	 */
	public static DataResultSet findIDInSystem(final String field, final String value) throws IdcClientException {
		final DataBinder request = getNewDataBinder();

		request.putLocal(IDC_SERVICE, GET_DATARESULTSET);
		request.putLocal(RESULT_NAME, RESULTS);
		request.putLocal(DATA_SOURCE, REVISIONS);
		request.putLocal(WHERE_CLAUSE, field + " like '" + value + "'");

		final DataBinder response = executeService(request, USER);
		return response.getResultSet(RESULTS);
	}

	/**
	 * Import content item using service IMPORT_DOCUMENT.
	 *
	 * @param dID
	 *            The dID to import
	 * @param dDocName
	 * @param aArchiveName
	 * @param aBatchFile
	 */
	public static void importDocument(final DataResultSetRow row, final List<Field> fields, final String aArchiveName,
			final String aBatchFile) throws IdcClientException {

		final DataBinder request = getNewDataBinder();

		request.putLocal(IDC_SERVICE, CFG_SERVICE);

		request.putLocal(ACTION, CFG_ACTION);
		request.putLocal(IDC_NAME, CFG_IDC_NAME);
		request.putLocal(A_ARCHIVE_NAME, aArchiveName);
		request.putLocal(A_BATCH_FILE, aBatchFile);

		for (Field field : fields) {
			String name = field.getName();

			if (!fieldsToExclude.contains(name)) {
				String val = row.get(name);
				if (name.equals(PRIMARY_FILE) || name.equals(WEB_VIEWABLE_FILE) || name.startsWith(D_RENDITION_1)
						|| name.startsWith(D_RENDITION_2)) {

					val = CFG_ARCHIVE_DIR + CFG_ARCHIVE_NAME + SLASH + val;
				}
				request.putLocal(name, val);
			}
		}

		System.out.println("Missing: dID=" + row.get(D_ID) + " dDocName=" + row.get(D_DOC_NAME));

		if (CFG_DO_IMPORT) {
			try {
				executeService(request, USER);
				// System.out.println("Success: dID=" + row.get(D_ID) + "
				// dDocName=" + row.get(D_DOC_NAME));
			} catch (IdcClientException e) {
				System.out.println("Failure to import: dID=" + row.get(D_ID) + " dDocName=" + row.get(D_DOC_NAME));
				System.err.println(e.getMessage());
			}
		}
	}
}