package com.jonathanhult.webcenter.content.archiverbatchutils;

import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchMain.CFG_ARCHIVE_NAME;
import static com.jonathanhult.webcenter.content.archiverbatchutils.ArchiverBatchStringConstants.D_DOC_NAME;

import java.util.List;

import oracle.stellent.ridc.IdcClientException;
import oracle.stellent.ridc.model.DataObject;
import oracle.stellent.ridc.model.DataResultSet;
import oracle.stellent.ridc.model.DataResultSet.Field;
import oracle.stellent.ridc.model.impl.DataResultSetRow;

public class FindAndImportWorker implements Runnable {

	final DataResultSetRow row;
	final List<Field> fields;
	final String aBatchFile;

	public FindAndImportWorker(DataResultSetRow row, List<Field> fields, String aBatchFile) {
		this.row = row;
		this.fields = fields;
		this.aBatchFile = aBatchFile;
	}

	@Override
	public void run() {
		DataResultSet revisions;
		try {
			revisions = BatchUtils.findIDInSystem(D_DOC_NAME, row.get(D_DOC_NAME));
			// System.out.println("dID=" + row.get("dID") + " dDocName=" +
			// row.get(D_DOC_NAME) + " dRevClassID="
			// + row.get("dRevClassID"));

			final List<DataObject> rowObj = revisions.getRows();
			if (rowObj.size() == 0) {
				BatchUtils.importDocument(row, fields, CFG_ARCHIVE_NAME, aBatchFile);
			}
		} catch (IdcClientException e) {
			e.printStackTrace(System.err);
		}
	}
}
