package com.jonathanhult.webcenter.content.archiverbatchutils;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ArchiverBatchStringConstants {

	// RIDC connection information
	public static final String IDC_PROTOCOL = "idc://";
	public static final String USER = "sysadmin";
	public static final String IDC_SERVICE = "IdcService";

	public static final String CFG_OPTIONS = "options.cfg";
	public static final String IDC_NAME = "IDC_Name";
	public static final String SERVER = "Server";
	public static final String PORT = "Port";
	public static final String ARCHIVE_NAME = "ArchiveName";
	public static final String ARCHIVE_DIR = "ArchiveDir";
	public static final String BATCH_START_NUMBER = "BatchStartNumber";
	public static final String ACTION = "Action";
	public static final String SERVICE = "Service";
	public static final String DO_IMPORT = "DoImport";

	public static final String D_DOC_NAME = "dDocName";
	public static final String D_ID = "dID";

	public static final String PRIMARY_FILE = "primaryFile";
	public static final String WEB_VIEWABLE_FILE = "webViewableFile";
	public static final String D_RENDITION_1 = "dRendition1:";
	public static final String D_RENDITION_2 = "dRendition2:";

	public static final String SLASH = "/";

	public static final String CHECKIN_UNIVERSAL = "CHECKIN_UNIVERSAL";

	public static final String GET_DATARESULTSET = "GET_DATARESULTSET";
	public static final String DATA_SOURCE = "dataSource";
	public static final String WHERE_CLAUSE = "whereClause";
	public static final String REVISIONS = "Revisions";
	public static final String RESULT_NAME = "resultName";
	public static final String RESULTS = "Results";
	public static final String START_ROW = "StartRow";
	public static final String NUM_ROWS = "NumRows";
	public static final String EXPORT_RESULTS = "ExportResults";

	// Batch info
	public static final String GET_ARCHIVED_FILE = "GET_ARCHIVED_FILE";
	public static final String GET_BATCHFILES = "GET_BATCHFILES";
	public static final String GET_BATCH_VALUES = "GET_BATCH_VALUES";
	public static final String GET_BATCH_FILE_DOCUMENTS = "GET_BATCH_FILE_DOCUMENTS";
	public static final String IMPORT_DOCUMENT = "IMPORT_DOCUMENT";
	public static final String A_ARCHIVE_NAME = "aArchiveName";
	public static final String A_BATCH_FILE = "aBatchFile";
	public static final String BATCH_FILES = "BatchFiles";
	public static final String BATCH_VALUES = "BatchValues";
	public static final String BATCH_FIELD_NAME = "batchFieldName";

	// Fields to exclude
	public static final String D_REV_LABEL = "dRevLabel";
	public static final String D_ORIGINAL_NAME = "dOriginalName";
	public static final String D_REV_CLASS_ID = "dRevClassID";
	public static final String D_IS_CHECKED_OUT = "dIsCheckedOut";
	public static final String D_CHECKOUT_USER = "dCheckoutUser";
	public static final String D_STATUS = "dStatus";
	public static final String D_RELEASE_STATE = "dReleaseState";
	public static final String D_FLAG_1 = "dFlag1";
	public static final String D_PROCESSING_STATE = "dProcessingState";
	public static final String D_MESSAGE = "dMessage";
	public static final String D_INDEXER_STATE = "dIndexerState";
	public static final String D_PUBLISH_STATE = "dPublishState";
	public static final String D_REVISION_ID = "dRevisionID";

	// Add dID to list if you wan get a new dID
	public static final String[] fieldsToExcludeArray = { D_IS_CHECKED_OUT, D_CHECKOUT_USER, D_STATUS, D_RELEASE_STATE,
			D_FLAG_1, D_PROCESSING_STATE, D_MESSAGE, D_INDEXER_STATE, D_PUBLISH_STATE, D_REV_CLASS_ID, D_REVISION_ID };
	public static final List<String> fieldsToExclude = new LinkedList<>(Arrays.asList(fieldsToExcludeArray));

}
